From: http://steamcommunity.com/sharedfiles/filedetails/?id=203603867

Add `-exec autoexec` to startup properties, or run in dev console manually.

Key Names:

Letter Keys
A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z

Number Keys
1,2,3,4,5,6,7,8,9,0

Function Keys
F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12

Arrow Keys
LEFTARROW
RIGHTARROW
UPARROW
DOWNARROW

Special Keys
ENTER, SPACE, SHIFT, CTRL, PAUSE, ALT, BACKSPACE, TAB, CAPSLOCK

Scroll/Navigation Keys
INS, DEL, HOME, END, PGUP, PGDN

Misc. Keys
, . / \ ; ' * [

KP_MULTIPLY - "*"
KP_SUBTRACT/MINUS - "-" 
KP_PLUS - "+"
KP_INS - 0 / Ins
KP_DEL - "." / Del
KP_END - 1 / End
KP_DOWNARROW - 2 / Down Arrow
KP_PGDN - 3 / Page Down
KP_LEFTARROW - 4 / Left Arrow
KP_5 - 5 (Centre Button)
KP_RIGHTARROW - 6 / Right Arrow
KP_HOME - 7 / Home
KP_UPARROW - 8 / Up Arrow
KP_PGUP - 9 / Page Up



Pistols:
glock - Glock 20 (T Only)
hkp2000 - P2000 
p250 - P250
elite - Dual Elites
deagle - Desert Eagle/Deagle
tec9 - Five Seven (CT Only)
tec9 - Tec 9 (T Only)

Sub-Machine Guns/PDW's:
mp9 - MP9 (CT Only)
mac10 - Mac 10 (T Only)
mp7 - MP7
p90 - P90
bizon - PP-19 Bizon
ump45 - UMP .45

Shotguns:
nova - Nova
xm1014 - XM 1014
mag7 - MAG 7 (CT Only)
sawedoff - Sawed Off (T Only)

Machine Guns:
negev - Negev
m249 - M249

Rifles:
sg008 - SG 008 Sniper/Scout
awp - AWP Sniper
ak47 - AK 47 (T Only)
galilar - Galil (T Only)
sg556 - SG 556 (T Only)
g3sg1 - G3 SG1 (T Only)
famas - Famas (CT Only)
m4a1 - M4A4 (CT Only)
m4a1-s - M4A1 Silenced (CT Only)
aug - AUG (CT Only)
scar20 - Scar 20 (CT Only)

Grenades:
hegrenade - Basic Grenade
flashbang - Flashbang
smokegrenade - Smoke Grenade
decoy - Decoy Grenade
molotov - Molotov (T Only)
incgrenade - Incindiary Grenade (CT Only)

Gear:
defuser - Defuse Kit
vest - Kevlar 
vesthelm - Kevlar+Helment
